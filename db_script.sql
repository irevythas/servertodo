CREATE USER 'test_user'@'localhost' IDENTIFIED BY 'user1234';
ALTER USER 'test_user'@'localhost' IDENTIFIED WITH mysql_native_password BY 'user1234';
GRANT ALL PRIVILEGES ON *.* TO 'test_user'@'localhost' WITH GRANT OPTION;

create database todo;
use todo;
create table MyToDoList (id int NOT NULL AUTO_INCREMENT, description varchar(255), PRIMARY KEY(id)) ;


