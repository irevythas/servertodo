package com.revythas.server.todo;

import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class TodoCtrl {

    private TodoRepository todoRepository;

    public TodoCtrl(TodoRepository todoRepository){
        this.todoRepository = todoRepository;
    }

    @GetMapping({"/", "/todos"})
    public List<Todo> findTodos() {
        return todoRepository
                .findAll()
                .stream()
                .sorted(Comparator.
                        comparing(Todo::getDescription))
                .collect(Collectors.toList());
    }


    @PostMapping("/todo")
    public void postTodo(@RequestBody Todo todo){
        todoRepository.save(todo);

    }

    /*
    @DeleteMapping("/todo")
    public void deleteTodo(@RequestBody Todo todo){
        todoRepository.delete(todo);

    }
    */

    @GetMapping("/todo/{id}")
    public void deleteById(@PathVariable Integer id){
        Todo t = todoRepository.findOne(id);
        todoRepository.delete(t);
    }


}
